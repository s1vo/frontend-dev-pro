import webpack from "webpack";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import { BuildOptions } from "./types/config";

// Функция buildLoaders принимает параметры сборки проекта и возвращает массив правил для загрузчиков webpack
export function buildLoaders({ isDev }: BuildOptions): webpack.RuleSetRule[] {

    // Загрузчик для обработки файлов стилей (CSS и SASS)
    const cssLoader = {
        test: /\.s[ac]ss$/i, // Проверяем, соответствует ли файл расширениям .scss, .sass или .css
        use: [
            // Используем style-loader в режиме разработки и MiniCssExtractPlugin.loader в продакшн режиме
            isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
            {
                loader: 'css-loader', // Загрузчик для обработки CSS-файлов
                options: {
                    modules: {
                        auto: (resPath: string) => Boolean(resPath.includes('.module.')), // Включаем CSS-модули для файлов, содержащих ".module."
                        localIdentName: isDev ? '[path][name]__[local]' : '[hash:base64:8]', // Формат имен классов в CSS-модулях
                    },
                }
            },
            'sass-loader', // Загрузчик для компиляции SASS в CSS
        ]
    };

    // Загрузчик для обработки TypeScript файлов
    const typescriptLoader = {
        test: /\.tsx?$/, // Проверяем, соответствует ли файл расширениям .ts или .tsx
        use: 'ts-loader', // Загрузчик для обработки TypeScript-файлов
        exclude: /node_modules/, // Исключаем папку node_modules из обработки TypeScript-файлов
    };

    return [
        typescriptLoader,
        cssLoader,
    ];
}
