import { ResolveOptions } from "webpack";

// Функция buildResolvers возвращает настройки разрешения файлов (расширений) для webpack
export function buildResolvers(): ResolveOptions {
    return {
        extensions: ['.tsx', '.ts', '.js'], // Указываем поддерживаемые расширения файлов (в порядке приоритета)
    };
}
