import { BuildOptions } from "./types/config";
import webpack from "webpack";
import path from "path";
import { buildPlugins } from "./buildPlugins";
import { buildLoaders } from "./buildLoaders";
import { buildResolvers } from "./buildResolvers";
import { buildDevServer } from "./buildDevServer";

// Функция buildWebpackConfig принимает параметры сборки проекта и возвращает конфигурацию webpack
export function buildWebpackConfig(options: BuildOptions): webpack.Configuration {
    const { paths, mode, isDev } = options;

    return {
        mode: mode, // Устанавливаем режим сборки (production или development) из параметров сборки
        entry: paths.entry, // Указываем точку входа для сборки проекта
        output: {
            filename: "[name].[contenthash].js", // Формат имен выходных JavaScript-файлов
            path: paths.build, // Указываем путь для сохранения собранного проекта
            clean: true, // Очищаем папку сборки перед каждой новой сборкой
        },
        plugins: buildPlugins(options), // Используем плагины сборки, созданные функцией buildPlugins
        module: {
            rules: buildLoaders(options), // Используем загрузчики сборки, созданные функцией buildLoaders
        },
        resolve: buildResolvers(), // Используем настройки разрешения файлов, созданные функцией buildResolvers
        devtool: isDev ? 'inline-source-map' : undefined, // Устанавливаем инструменты для отладки в режиме разработки
        devServer: isDev ? buildDevServer(options) : undefined, // Используем настройки сервера разработки, если проект собирается в режиме разработки
    };
}
