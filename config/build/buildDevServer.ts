import { BuildOptions } from "./types/config";
import type { Configuration as DevServerConfiguration } from "webpack-dev-server";

// Функция buildDevServer принимает параметры сборки проекта и возвращает конфигурацию сервера разработки для webpack-dev-server
export function buildDevServer(options: BuildOptions): DevServerConfiguration {
    return {
        port: options.port, // Устанавливаем порт сервера разработки из параметров сборки
        open: true, // Опция, указывающая, следует ли автоматически открывать браузер при запуске сервера
        historyApiFallback: true,
    };
}