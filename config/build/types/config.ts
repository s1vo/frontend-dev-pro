// Определяем тип данных для режима сборки, который может быть 'production' или 'development'
export type BuildMode = 'production' | 'development';

// Интерфейс для путей к файлам, необходимым для сборки проекта
export interface BuildPaths {
    entry: string; // Путь к файлу с точкой входа
    build: string; // Путь для сохранения собранного проекта
    html: string; // Путь к HTML-файлу, связанному с проектом
}
// Интерфейс для переменных окружения, связанных с процессом сборки
export interface BuildEnv {
    mode: BuildMode; // Режим сборки: 'production' или 'development'
    port: number; // Порт, на котором будет запущен сервер разработки
}

// Интерфейс для параметров сборки проекта
export interface BuildOptions {
    mode: BuildMode; // Режим сборки: 'production' или 'development'
    paths: BuildPaths; // Пути к файлам, связанным с сборкой проекта
    isDev: boolean; // Флаг, указывающий, что проект собирается в режиме разработки
    port: number; // Порт, на котором будет запущен сервер разработки или сервер для обслуживания собранного проекта
}
