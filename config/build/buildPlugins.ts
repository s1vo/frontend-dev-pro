import HTMLWebpackPlugin from "html-webpack-plugin";
import path from "path";
import webpack from "webpack";
import { BuildOptions } from "./types/config";
import MiniCssExtractPlugin from "mini-css-extract-plugin";

// Функция buildPlugins принимает параметры сборки проекта и возвращает массив плагинов webpack
export function buildPlugins({ paths }: BuildOptions): webpack.WebpackPluginInstance[] {

    return [
        // Плагин для генерации HTML-файла на основе указанного шаблона
        new HTMLWebpackPlugin({
            template: paths.html, // Указываем путь к HTML-шаблону
        }),
        // Плагин для отображения прогресса сборки в консоли
        new webpack.ProgressPlugin(),
        // Плагин для извлечения CSS в отдельные файлы
        new MiniCssExtractPlugin({
            filename: 'css/[name].[contenthash:8].css', // Указываем путь и формат имен выходных CSS-файлов
            chunkFilename: 'css/[name].[contenthash:8].css', // Указываем формат имен выходных CSS-файлов для чанков
        }),
    ];
}
