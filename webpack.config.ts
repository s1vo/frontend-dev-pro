import webpack from 'webpack';
import { buildWebpackConfig } from "./config/build/buildWebpackConfig";
import { BuildEnv, BuildPaths } from "./config/build/types/config";
import path from "path";
// Экспортируем функцию, которая принимает объект env с переменными окружения
export default (env: BuildEnv) => {
    // Определяем пути к файлам проекта
    const paths: BuildPaths = {
        entry: path.resolve(__dirname, 'src', 'index.tsx'), // Путь к файлу с точкой входа
        build: path.resolve(__dirname, 'build'), // Путь для сохранения собранного проекта
        html: path.resolve(__dirname, 'public', 'index.html'), // Путь к HTML-файлу, связанному с проектом
    };
    // Определяем режим сборки и флаг режима разработки
    const mode = env.mode || 'development';
    const isDev = mode === 'development';
    // Определяем порт для сервера разработки
    const PORT = env.port || 3000;
    // Создаем конфигурацию webpack, используя функцию buildWebpackConfig
    const config: webpack.Configuration = buildWebpackConfig({
        mode,
        paths,
        isDev,
        port: PORT,
    });
    return config; // Возвращаем конфигурацию webpack
};
